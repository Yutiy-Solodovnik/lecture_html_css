# This homework is to create a web page following the example. I note right away that this work assumes the use of only HTML and CSS (no JS!).

And so, the task: to create a layout of a streaming service for the Binary Studio of a cat - Bunka. This is how it should look:   

And also an overview of the page: video    

## Detailed layout requirements:    
## Header   
### Drop-down menu:   

Always placed at the top of the window when scrolling the page    
Has the same left and right margins    
On the left is the logo    
The search bar and voice search icon are located in the center.    
- When the search line is focused, a border is added to the input    
- When the window is reduced, the width of the search line decreases    
On the right is the icon of notifications and user profile   
- When you hover over the user icon, a drop-down menu should appear    
- When you hover over a menu item, its background color should change    
### Side panel   
 
## Information block:

Has a fixed width and takes up the entire available height   
When the window width is less than 1200px, the panel width is reduced (accommodating only streamer avatars)   
The panel consists of two blocks: a block with streamers (scrollable - becomes "scrollable" when there are a large number of elements), and an information block - has a fixed height.   
The panel has the same left and right margins as the header    
Information block contains text (center alignment) and social media icons    
- When text is selected, the highlight color and the text color change   
- When you hover over the icons, their color changes    
The block of streamers contains a list of cards with information about the streamer   
- When you hover over a card, its background changes   
- The card contains an avatar and name (left) and an activity indicator and number of viewers / subscribers (right). The activity indicator has a pulse animation   
- The avatar has a round shape   
- For long name use text-overflow: ellipsis;   

## Main panel:
### Main panel:

Takes up all free space, scrollable, has the same indentations as in the header   
Consists of three parts:   

### Panel with categories:

Has the same height as the first card from the sidebar   
The panel is horizontally scrollable with a large number of elements    
The distance between the categories is the same    
The category has a rounded border    
The first category has a different color and background (no need to write a separate style for this)   
It is advisable to use grid for this part.   

### Carousel: 

The carousel should contain a video preview (the same block as in the video panel) and a description panel on the right   
The description panel should contain an avatar, video title, streamer name (the same block as in the video) and description   

### Video panel:

Contains video blocks   
The vertical spacing between blocks must be the same   
The horizontal spacing between blocks must be the same   
When decreasing the page size, the number of columns should decrease   
The layout of this block should be responsive and adaptive.   
It is advisable to use grid for this block.  
 
### Video container

Consists of two parts: preview and description    
The preview contains an image    
The preview image contains the "Live" badge and the number of active viewers, or (if it is a recording) - the duration of the recording.   
With: hover, the "Play" icon appears on the preview and the image is darkened. This is all done with fadeIn animation.   
The description block contains the streamer's avatar, the video / broadcast name, the streamer's name, and the options icon on the right. Long names apply text-overflow: ellipsis. When you hover over the text, its color and the appearance of the cursor change.   

## Footer:

(Part of the main panel)   
Always at the bottom of the page (even if there is no video)   
Contains center-aligned text   
Has a deviation from the top (from the video block)    